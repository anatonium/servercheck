#!/bin/bash
function serverCheck {
  curl -sI $url -w '%{response_code}' -o /dev/null
}
IFS=$'\n'
for url in $(cat urls.txt)
do
  if [ "$(serverCheck)" -ge 400 ]
then
  echo $(serverCheck)'@'$url
  break
else 
  echo $(serverCheck)'@'$url
fi
done
